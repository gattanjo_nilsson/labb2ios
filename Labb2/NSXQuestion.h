//
//  NSXQuestion.h
//  Labb2
//
//  Created by ITHS on 2016-02-03.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSXQuestion : NSObject

    @property NSString *question;
    @property NSString *answerA;
    @property NSString *answerB;
    @property NSString *answerC;
    @property NSString *answerD;
    @property NSString *correctAnswer;


@end
