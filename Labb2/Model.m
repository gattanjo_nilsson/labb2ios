//
//  Model.m
//  Labb2
//
//  Created by ITHS on 2016-02-03.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "Model.h"
#import "NSXQuestion.h"

@interface Model()
@property(nonatomic) NSMutableArray *questions;

@end

@implementation Model

-(instancetype)init {
    self = [super init];
    if(self) {
        
        self.questions  = [[NSMutableArray alloc] init];
        
        NSXQuestion *q1 = [[NSXQuestion alloc] init];
        q1.question = @"Who is the creator of Super Mario?";
        q1.answerA = @"Bill Gates";
        q1.answerB = @"Steve Jobs";
        q1.answerC = @"Satoru Iwata";
        q1.answerD = @"Shigeru Miyamoto";
        q1.correctAnswer = @"Shigeru Miyamoto";
        
        NSXQuestion *q2 = [[NSXQuestion alloc] init];
        q2.question = @"How many Pokémons are there in the first generation of Pokémon?";
        q2.answerA = @"150";
        q2.answerB = @"200";
        q2.answerC = @"151";
        q2.answerD = @"101";
        q2.correctAnswer = @"151";
        
        NSXQuestion *q3 = [[NSXQuestion alloc] init];
        q3.question = @"How many players can play Super Smash Bros Wii U simultaneously";
        q3.answerA = @"1";
        q3.answerB = @"2";
        q3.answerC = @"4";
        q3.answerD = @"8";
        q3.correctAnswer = @"8";
        
        NSXQuestion *q4 = [[NSXQuestion alloc] init];
        q4.question = @"Who created the Gameboy";
        q4.answerA = @"Gunpei Yokoi";
        q4.answerB = @"Satoru Iwata";
        q4.answerC = @"Shigeru Miyamoto";
        q4.answerD = @"Hiroguchi Yamauchi";
        q4.correctAnswer = @"Gunpei Yokoi";
        
        NSXQuestion *q5 = [[NSXQuestion alloc] init];
        q5.question = @"Which hobby became the inspiration to Pokémon?";
        q5.answerA = @"Collecting cards";
        q5.answerB = @"Collecting insects";
        q5.answerC = @"Dungeons and Dragons";
        q5.answerD = @"Animal torture";
        q5.correctAnswer = @"Collecting insects";
        
        NSXQuestion *q6 = [[NSXQuestion alloc] init];
        q6.question = @"In which year was Final Fantasy VII released?";
        q6.answerA = @"1998";
        q6.answerB = @"1997";
        q6.answerC = @"1996";
        q6.answerD = @"1999";
        q6.correctAnswer = @"1997";
        
        NSXQuestion *q7 = [[NSXQuestion alloc] init];
        q7.question = @"Who is the main character in Halo?";
        q7.answerA = @"Master Chef";
        q7.answerB = @"Roboguy";
        q7.answerC = @"Master Chief";
        q7.answerD = @"Private Ryan";
        q7.correctAnswer = @"Master Chief";
        
        NSXQuestion *q8 = [[NSXQuestion alloc] init];
        q8.question = @"Who is the creator of Half-Life?";
        q8.answerA = @"Gabe Newell";
        q8.answerB = @"Gabe Newegg";
        q8.answerC = @"Gabe Macaroni";
        q8.answerD = @"Gabe Syzlac";
        q8.correctAnswer = @"Gabe Newell";
        
        NSXQuestion *q9 = [[NSXQuestion alloc] init];
        q9.question = @"What is the official Final Fantasy Orchestral Concerts called?";
        q9.answerA = @"Final Fantasy Live";
        q9.answerB = @"Distant Worlds";
        q9.answerC = @"Final Symphony";
        q9.answerD = @"Final Fantasy Orchestra";
        q9.correctAnswer = @"Distant Worlds";
        
        NSXQuestion *q10 = [[NSXQuestion alloc] init];
        q10.question = @"What is Super Mario's lastname?";
        q10.answerA = @"Luigi";
        q10.answerB = @"Mario";
        q10.answerC = @"Linguini";
        q10.answerD = @"Donati";
        q10.correctAnswer = @"Mario";
        
        [self.questions addObject:q1];
        [self.questions addObject:q2];
        [self.questions addObject:q3];
        [self.questions addObject:q4];
        [self.questions addObject:q5];
        [self.questions addObject:q6];
        [self.questions addObject:q7];
        [self.questions addObject:q8];
        [self.questions addObject:q9];
        [self.questions addObject:q10];
        
    }
    return self;
}

- (BOOL)selected:(NSString*)answer andQuestion:(NSXQuestion*)question{
     BOOL correct;
    ++self.round;
    if([question.correctAnswer isEqualToString:answer]){
        ++self.score;
        correct = YES;
    }
    else {
        ++self.incorrect;
        correct = NO;
    }
     return correct;
}

- (NSXQuestion*)nextQuestion {
    
    return self.questions[arc4random() % self.questions.count];
    
}

-(void) viewDidLoad {
    
}

@end
