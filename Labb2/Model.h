//
//  Model.h
//  Labb2
//
//  Created by ITHS on 2016-02-03.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSXQuestion.h"

@interface Model : NSObject

@property int score;
@property int incorrect;
@property int round;

- (BOOL)selected:(NSString*)answer andQuestion:(NSXQuestion*)question;
- (NSXQuestion*)nextQuestion;

@end
