//
//  ViewController.m
//  Labb2
//
//  Created by ITHS on 2016-02-03.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "ViewController.h"
#import "Model.h"
#import "ResultViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *score;
@property (strong, nonatomic) Model *model;
@property (strong, nonatomic) NSXQuestion *q;
@property (strong, nonatomic) NSMutableSet *used;
@property (weak, nonatomic) IBOutlet UIButton *alternativeA;
@property (weak, nonatomic) IBOutlet UIButton *alternativeB;
@property (weak, nonatomic) IBOutlet UIButton *alternativeC;
@property (weak, nonatomic) IBOutlet UIButton *alternativeD;

@end

@implementation ViewController

-(Model*)model {
    if(!_model) {
        _model = [[Model alloc] init];
    }
    return _model;
}

-(NSXQuestion*)q {
    if(!_q) {
        _q = [[NSXQuestion alloc] init];
    }
    return _q;
}
-(NSMutableSet*)used {
    if(!_used) {
        _used = [[NSMutableSet alloc] init];
    }
    return _used;
}

- (IBAction)alternativeA:(id)sender {
    [self isAnswerCorrect:[self.model selected:self.q.answerA andQuestion:self.q]];
}
- (IBAction)alternativeB:(id)sender {
    [self isAnswerCorrect:[self.model selected:self.q.answerB andQuestion:self.q]];
}
- (IBAction)alternativeC:(id)sender {
    [self isAnswerCorrect:[self.model selected:self.q.answerC andQuestion:self.q]];
}
- (IBAction)alternativeD:(id)sender {
    [self isAnswerCorrect:[self.model selected:self.q.answerD andQuestion:self.q]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self changeQuestion];
}

-(void) isAnswerCorrect:(BOOL)correct {
    if(correct == YES){
        self.questionLabel.textColor = [UIColor greenColor];
        self.questionLabel.text = @"Correct";
    }
    else {
        self.questionLabel.textColor = [UIColor redColor];
        self.questionLabel.text = @"Incorrect";
    }
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerCalled) userInfo:nil repeats:NO];
}

-(void)timerCalled
{
    [self changeQuestion];
}

-(void)changeQuestion {
    self.questionLabel.textColor = [UIColor blackColor];
    if(self.model.round == 5) {
        [self performSegueWithIdentifier:@"next" sender:self];
    }
    self.q = [self.model nextQuestion];
    
    if([self.used containsObject:self.q.question]) {
        [self changeQuestion];
    }
    else {
        [self.used addObject:self.q.question];
        self.questionLabel.text = self.q.question;
        [self.score setText:[NSString stringWithFormat:@"%d", self.model.score]];
        [self.alternativeA setTitle:self.q.answerA forState:UIControlStateNormal];
        [self.alternativeB setTitle:self.q.answerB forState:UIControlStateNormal];
        [self.alternativeC setTitle:self.q.answerC forState:UIControlStateNormal];
        [self.alternativeD setTitle:self.q.answerD forState:UIControlStateNormal];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"next"]){
        ResultViewController *controller = (ResultViewController *) segue.destinationViewController;
        controller.correctText = [NSString stringWithFormat:@"%d", self.model.score];
        controller.incorrectText = [NSString stringWithFormat:@"%d", self.model.incorrect];
    }
}


@end
