//
//  ViewController.h
//  Labb2
//
//  Created by ITHS on 2016-02-03.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface ViewController : UIViewController

-(void)changeQuestion;

-(void) isAnswerCorrect:(BOOL)correct;

-(void)timerCalled;

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@end

