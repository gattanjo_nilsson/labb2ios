//
//  ResultViewController.m
//  Labb2
//
//  Created by ITHS on 2016-02-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "ResultViewController.h"

@interface ResultViewController ()
@property (weak, nonatomic) IBOutlet UILabel *correctLabel;
@property (weak, nonatomic) IBOutlet UILabel *incorrectLabel;

@end

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.correctLabel.text = self.correctText;
    self.incorrectLabel.text = self.incorrectText;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
