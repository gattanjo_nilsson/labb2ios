//
//  ResultViewController.h
//  Labb2
//
//  Created by ITHS on 2016-02-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface ResultViewController : UIViewController

@property(nonatomic) NSString *correctText;
@property(nonatomic) NSString *incorrectText;

@end
